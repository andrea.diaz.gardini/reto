const express = require('express');
const path = require('path');
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackConfig from './webpack.config.js';

// initializations
const app  = express();

// settings
app.set('port', 8000);


// middlewares
app.use(webpackDevMiddleware(webpack(webpackConfig)));



// routes
app.use(express.static(__dirname + '/public'));

  // index
  // app.get('/', function (req, res) {
  //   res.sendFile(path.join(app.get('nA') + '/main.html'));
  // });


// starting the server
app.listen(app.get('port'), () => {
  console.log('server on port', app.get('port'));
});
