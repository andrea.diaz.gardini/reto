import webpack from 'webpack';
import htmlWebpackPlugin from 'html-webpack-plugin';
import LiveReloadPlugin from 'webpack-livereload-plugin';


module.export = {
  entry: './src/index.js',
  output: {
    path: __dirname +  '/public/js',
    filename: 'bundle.js'
  },

  module: {
    rules: [
      {
        use: 'babel-loader',
        test: /\.js$/,
        exclude: /node_modules/,
      },
      {
        use: ['css-loader', 'style-loader'],
        test: /\.css$/,
      },

    ]
  },

  plugins: [
    new htmlWebpackPlugin({
      template: './public/index.html'
    }),
    new LiveReloadPlugin()
  ]
}
